# Licensing information {#license}

This version of CellGPU is licensed und GPLv3

If want to use CellGPU with the original MIT license, you need to go to
https://github.com/sussmanLab/cellGPU

Copyright (c) 2016 - 2019 Daniel M. Sussman
Copyright (c) 2021        Alois Schloegl

This version of CellGPU is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


# External code used

Additionally, some files and functionality draw from existing open-source code, as described below.

(1) Two files (gpuarray.h and indexer.h) are largely based on parts of the HOOMD-blue project, released
under the BSD 3-Clause License.
https://glotzerlab.engin.umich.edu/hoomd-blue

HOOMD-blue Open Source Software License Copyright 2009-2016 The Regents of
the University of Michigan All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions, and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions, and the following disclaimer both in the code and prominently in any materials provided with the distribution.
* Neither the name of the copyright holder nor the names of its contributors may be used to enorse or promote products derived from this software without specific prior written permission

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND/OR
ANY WARRANTIES THAT THIS SOFTWARE IS FREE OF INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
THE POSSIBILITY OF SUCH DAMAGE.

(2) The file HilbertSort.h calls, but does not modify, code released under the GNU LGPL licence by John
Burkardt. The "HilberSort.h" wrapper is used by the program to call the functions defined in the
library, and the source code from which the library can be built is in the "hilbert_sort.hpp" and
"hilbert_sort.cpp" files. Thus, this repository contains everything a user would need to relink the
application with a different version of Burkardts LGPL source code. As such CellGPU can be distributed
under a non-(L)GPL license. Credit for this library, of course, goes to John Burkardt:
https://people.sc.fsu.edu/~jburkardt/cpp_src/hilbert_curve/hilbert_curve.html

(3) eigenMatrixInterface.h and .cpp interfaces with the Eigen library. Eigen is Free Software,
available from eigen.tuxfamily.org/. It is licensed under the MPL2. See https://www.mozilla.org/en-US/MPL/2.0/ for more details.

(4) Finally, the cellGPU logo was made by using the ``Lincoln Experiments'' project,
https://snorpey.github.io/triangulation/ released by Georg Fischer under the MIT license
(Copyright 2013 Georg Fischer). The image used was taken by Torsten Wittmann and is public domain,
available from http://www.cellimagelibrary.org/images/240
